import Home from "../../pages/home/Home";
import Login from "../../pages/login/Login";
import Register from "../../pages/register/Register";
import Write from "../../pages/write/Write";
import Settings from "../../pages/settings/Settings";
import SinglePost from "../singlePost/SinglePost";
import { Route, Routes } from "react-router-dom";

export default function RoutePath() {
  return (
    <div>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/write" element={<Write />} />
        <Route exact path="/settings" element={<Settings />} />
        <Route exact path="/login" element={<Login />} />
        <Route exact path="/register" element={<Register />} />
        <Route exact path="/post/:id" element={<SinglePost />} />
      </Routes>
    </div>
  );
}
