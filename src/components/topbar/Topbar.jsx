import { Link } from "react-router-dom";
import "./topbar.css";

export default function Topbar() {
  let user = true;
  return (
    <div className="topbar">
      <div className="topLeft">
        <h2 className="topHead">ActBlog</h2>
      </div>
      <div className="topCenter">
        <ul className="topList">
          <Link to="/" className="topbarLink">
            Home
          </Link>
          <Link to="/about" className="topbarLink">
            About
          </Link>
          <Link to="/home" className="topbarLink">
            Contact US
          </Link>
          <Link to="/write" className="topbarLink">
            Write
          </Link>
          <Link to="/login" className="topbarLink">
            LogOut
          </Link>
        </ul>
      </div>
      <div className="topRight">
        {user ? (
          <img
            src="https://images.unsplash.com/photo-1683269615819-e38bcb9a3123?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
            alt="top-right-img"
            className="topImg"
          />
        ) : (
          <Link to="/login" className="topbarLink">
            LogIn
          </Link>
        )}
        <i className="topSearchIcon fas fa-search"></i>
      </div>
    </div>
  );
}
