import "./post.css";

export default function Post() {
  return (
    <div className="post">
      <img
        src="https://images.freeimages.com/clg/images/31/311968/free-vector-summer-beach-image_f.jpg"
        alt="postImg"
        className="postImg"
      />
      <div className="postInfo">
        <div className="postCats">
          <span className="postCat">Music</span>
          <span className="postCat">Life</span>
        </div>
        <span className="postTitle">Lorem ipsum dolor sit</span>
        <hr />
        <span className="postDate">1 hr ago</span>
      </div>
      <p className="postDesc">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos, doloremque
        accusantium excepturi modi temporibus incidunt nisi delectus molestias
        quasi consequuntur commodi voluptatum fuga at dignissimos itaque
        aspernatur pariatur assumenda dicta! Lorem ipsum dolor sit amet
        consectetur adipisicing elit. Eos, doloremque accusantium excepturi modi
        temporibus incidunt nisi delectus molestias quasi consequuntur commodi
        voluptatum fuga at dignissimos itaque aspernatur pariatur assumenda
        dicta! Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos,
        doloremque accusantium excepturi modi temporibus incidunt nisi delectus
        molestias quasi consequuntur commodi voluptatum fuga at dignissimos
        itaque aspernatur pariatur assumenda dicta!
      </p>
    </div>
  );
}
