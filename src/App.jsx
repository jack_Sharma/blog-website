import "./App.css";
import RoutePath from "./components/routes/RoutePath";
import Topbar from "./components/topbar/Topbar";
import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <Router>
      <Topbar />
      <RoutePath />
    </Router>
  );
}

export default App;
