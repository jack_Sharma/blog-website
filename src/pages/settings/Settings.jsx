import "./settings.css";
import Sidebar from "../../components/sidebar/Sidebar";

export default function Settings() {
  return (
    <div className="settings">
      <div className="settingsWrapper">
        <div className="settingsTitle">
          <span className="settingsUpdateTitle">Update your account</span>
          <span className="settingsDeleteTitle">Delete Account</span>
        </div>
        <form className="settingsForm">
          <p className="settingsFormPPHead">Profile Picture</p>
          <div className="settingsPP">
            <img
              src="https://images.unsplash.com/photo-1683269615819-e38bcb9a3123?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
              alt="profilePic"
              className="profilePic"
            />
            <label htmlFor="fileInput">
              <i className="settingsPPIcon far fa-user-circle"></i>
            </label>
            <input type="file" id="fileInput" style={{ display: "none" }} />
          </div>
          <div className="settingsUserDetails">
            {/* <label>Username</label> */}
            <input type="text" placeholder="user_name" />
            {/* <label>Email</label> */}
            <input type="email" placeholder="email" />
            {/* <label>Password</label> */}
            <input type="password" placeholder="password" />
            <button className="settingsSubmit">Update</button>
          </div>
        </form>
      </div>
      <Sidebar />
    </div>
  );
}
