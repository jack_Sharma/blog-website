import { Outlet } from "react-router-dom";
import Header from "../../components/header/Header";
import Posts from "../../components/posts/Posts";
import Sidebar from "../../components/sidebar/Sidebar";
import "./home.css";
import Login from "../login/Login";
import { useState } from "react";

export default function Home() {
  const [user, setUser] = useState(true);
  return (
    <>
      {user ? (
        <>
          <Header />
          <div className="home">
            <Posts />
            <Sidebar />
          </div>
        </>
      ) : (
        <>
          <Login user={user} setUser={setUser} />
        </>
      )}

      <Outlet />
    </>
  );
}
